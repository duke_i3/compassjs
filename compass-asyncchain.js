/** 
 * Compass-AsyncChain.js 
 * by i3-systems hitoshi douke.
 */

(function(){

if( window.Compass ) {
    Compass.namespace('Compass.AsyncChain');
    Compass.namespace('Compass.AsyncHelper');
    Compass.namespace('Compass.AsyncHelper.Callback');
} else {
    window.Compass = function(){};
}

/**
 * スコープチェーン登録用のクラス。
 * Compass.AsyncHelperクラスと合わせて利用する。
 */
Compass.AsyncChain = function(){
    this.processMap = {};
    this.scopeOwner = this;
};

/**
 * スコープチェーン登録オブジェクトを生成する。
 * このメソッドは一度土だけ呼び出してAsyncHelperオブジェクトを取得する。
 */
Compass.AsyncChain.prototype.create = function() {
    return new Compass.AsyncHelper(this, this.processMap);
};

/**
 * スコープチェーン処理を開始する。
 */
Compass.AsyncChain.prototype.start = function() {
    try {
        this.processMap.process.task.apply(this.scopeOwner, arguments);
    }catch(e) {
        alert(e);
        console.log(e);
    }
};

/**
 * スコープチェーンを終了し、破棄する。
 * 現在は処理未実装。
 */
Compass.AsyncChain.prototype.finish = function() {
    // nothing
    // パフォーマンスに影響しそうなものがあれば事後処理を行なう。
    return true;
};

/**
 * スコープチェーンを破棄する。
 * 現在は処理未実装。
 */
Compass.AsyncChain.prototype.destroy = function() {
    this.processMap = {};
};

/**
 * スコープを設定する。空指定の場合はデフォルトスコープを使う。
 * 本メソッドはAsyncChain.AsyncHelperにも定義して呼び出し可能にする。
 * EX:
 * var chain = new Compass.AsyncChain();
 * process = chain.create();
 * process.scope($page).async(func);
 * var callback = process.callback(0);
 * callback.scope(otherObj).task(func2).next().scope().task(func3);
 * # 流れる書き方
 * new Compass.AsyncChain().scope(this).create().async(task, f1, s1, f2).callback(1).task(f2).start();
 */
Compass.AsyncChain.prototype.scope = function(owner) {
    this.scopeOwner = owner ? owner : null;
    return this;
};


/**
 * 非同期処理、同期処理のスコープチェーンを行なう為のクラス。
 * Compass.AsyncChainクラスと合わせて利用する。
 */
Compass.AsyncHelper = function(builder,parentProcess){
    this.builder = builder;
    this.scopeOwner = builder.scopeOwner;
    this.callbacks = {};

    // ex: async.processMap.callbacks.success
    if(!parentProcess.process) {
        parentProcess.process = {task: null, callbacks: {}};
    }
    this.currentProcess = parentProcess.process;
    // this.isAsync = false;
};

/**
 * AsyncHelperに任意のスコープを設定する。
 * 空指定の場合はデフォルトスコープを使う。
 * @see {Async#scope}
 */
Compass.AsyncHelper.prototype.scope = function(owner) {
    this.scopeOwner = owner ? owner : null;
    return this;
};

/**
 * プロセスビルダーを開始する。
 * 処理はルートプロセスから実行される。
 */
Compass.AsyncHelper.prototype.start = function() {
    this.builder.start();
};

/**
 * 非同期処理を登録する。
 * 引数に指定した関数の後に、関数に渡す引数を可変で指定可能。
 * 可変引数の中でFunctionとして渡されたものは第一引数に指定した関数のコールバック関数として追加登録される。
 * EX: asyncHelper.async(funcA, 'hoge', function(){}, function(){});
 */
Compass.AsyncHelper.prototype.async = function(func) {
    // if(this.isAsync) {
    //     throw new Error('this process already async callback reserved.');
    // }
    var args = arguments.length > 1 ? Array.prototype.slice.call(arguments, 1, arguments.length) : [];
    // this.isAsync = true;
    return this.push(func, args, true);
};

/**
 * 同期処理を登録する。
 * 引数に指定した関数の後に、関数に渡す引数を可変で指定可能。
 * 非同期処理との違いは、可変引数の中でFunctionとして渡されたものがコールバック関数として登録されない。
 * よってコールバック関数登録処理 async.callback の呼出しを行なうと例外が発生する。
 * EX: asyncHelper.task(funcA, 'hoge', function(){}, function(){});
 */
Compass.AsyncHelper.prototype.task = function(func) {
    var args = Array.prototype.slice.call(arguments, 1, arguments.length);
    return this.push(func, args, false);
};

/**
 * プライベート関数として利用される。
 * 処理の登録を行なう。
 */
Compass.AsyncHelper.prototype.push = function(func, args, async) {
    var me = this;

    // 同期の場合はコールバックは無し。
    if(!async) {
        me.currentProcess.task = function(){
            var scope = me.scopeOwner || this;
            func.apply(scope, args);
        };
        return me;
    }

    // 非同期の場合はインターセプトしてコールバックを実行する関数を設定する。
    var iProcess = me.intercept_process_to_process(func, args);
    me.currentProcess.task = iProcess;

    return me;
};


/**
 * コールバックオブジェクトを返却する。
 */
Compass.AsyncHelper.prototype.callback = function(index) {
    var me = this;

    if( ! me.currentProcess.callbacks[index] ) {
        throw new Error("callback not found:"+index);
    }

    if( ! me.callbacks[index] ) {
        me.callbacks[index] = new Compass.AsyncHelper.Callback(me, index);
    }

    return me.callbacks[index];
};


/**
 * プライベート関数として利用される。
 * コールバック処理のスコープチェーン登録を行なう。
 */
Compass.AsyncHelper.prototype.intercept_process_to_process = function(func, args) {
    var me = this;
    var len = args.length;
    var current_callback = me.currentProcess.callbacks;
    var callback_index = 0;
    for(var i=0; i<len; i++) {
        if(typeof args[i]  === 'function') {
            current_callback[callback_index] = {};
            current_callback[callback_index].func = args[i];
            callback_index++;
        }
    }

    return function () {
        var callback_index = 0;
        for(var i=0; i<len; i++) {
            if(typeof args[i] === 'function') {
                args[i] = me.intercept(me.currentProcess.callbacks[callback_index]);
                callback_index++;
            }
        }
        var scope = me.scopeOwner || this;
        var result = func.apply(scope, args);

        return result;
    };
};

/**
 * プライベート関数として利用される。
 * コールバック処理のスコープチェーン登録を行なうためのサブルーチン。
 */
Compass.AsyncHelper.prototype.intercept = function(callback) {
    var me = this;
    var func = callback.func;
    var _func = callback.process ? callback.process.task : func;

    return function () {
        var result = _func.apply(this,　arguments);
        return result;
    };
};



/**
 * コールバッククラス
 * コールバック処理の実行と、次のタスク登録手続きを行うためのクラス。
 * AsyncHelperクラスにより利用される。
 */
Compass.AsyncHelper.Callback = function(helper, callbackIindex) {
    this.helper = helper;
    this.scopeOwner = helper.scopeOwner;
    this.callback = this.helper.currentProcess.callbacks[callbackIindex];

    this.callback.process = {
        task: null,
        next: null//, 
        // process: {}
    };
    this.task(function(){});
};


/**
 * コールバックタスクを実行するスコープを設定する。
 */
Compass.AsyncHelper.Callback.prototype.scope = function(owner) {
    this.scopeOwner = owner ? owner : null;
};


/**
 * コールバックタスクを登録する。
 * ここで登録したタスク実行後に新たなタスクを実行する場合は、
 * {#next}メソッド呼び出しを行い、新しいAsyncHelperを取得する。
 */
Compass.AsyncHelper.Callback.prototype.task = function(func) {
    var me = this;
    me.callback.process.task = function() { 
        var scope = me.scopeOwner || this;
        var args = Array.prototype.slice.call(arguments);
        me.result = func.apply(scope, args);

        var next = me.callback.process.next ? me.callback.process.next : null;  
        if(next) {
            me.result = next.apply(scope,[]);
        }
        return me.result;
    };

    return this;
};


/**
 * 次のプロセスを登録するためのメソッド。
 * 新しいAscyncHelperオブジェクトを返却する。
 */
Compass.AsyncHelper.Callback.prototype.next = function() {
    var me = this;
    var builder = me.helper.builder;
    var currentProcess = me.callback.process;
    var nextHelper = new Compass.AsyncHelper(builder, currentProcess);

    me.callback.process.next = function() {
        if(currentProcess.process.task) {
            // currentProcess.process.task.apply(nextHelper.scopeOwner, [me.result]);
            currentProcess.process.task.apply(nextHelper.scopeOwner, arguments);
        }else{
            console.log("no next");
        }
        return "(this is next dummy result)";
    };

    return nextHelper;
};



})();